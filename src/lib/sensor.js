import dotenv from 'dotenv';
import * as dht from 'node-dht-sensor';

dotenv.config();

const ENV = process.env.NODE_ENV;
const SCALE = process.env.SCALE === undefined ? 'C' : process.env.SCALE;

let input = dht;

// fake sensor when not in prod (random values)
if (ENV !== 'production') {
    input = {
        read: function (type, pin) {
            return {
                temperature: 10 * Math.random() + 10,
                humidity: 10 * Math.random() + 70
            }
        }
    }
}

const sensor = {
    config: {
        type: 22,
        pin: 4,
    },
    read: function () {
        const values = input.read(this.config.type, this.config.pin);
        const conversion = (c) => SCALE === 'F' ? (c * 9 / 5) + 32 : c;
        return {
            temperature: conversion(values.temperature).toFixed(1),
            humidity: values.humidity.toFixed(1),
        };
    },
};


export default sensor;