const { env } = _process;
const URL = `http://${env.HOST}:${env.PORT}/read`
const INTERVAL = env.INTERVAL ?? 10000;

const temperature = document.getElementById('temperature');
const humidity = document.getElementById('humidity');

(async function read() {
    const response = await fetch(URL);

    if (response.ok) {
        const json = await response.json();

        temperature.innerHTML = `${json.temperature}<span>&deg;</span>`;
        humidity.innerHTML = `${json.humidity}<span><sup>%</sup></span>`;
    }
    setTimeout(read, INTERVAL);
})();