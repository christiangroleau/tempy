import dotenv from 'dotenv';
import express from 'express';
import sensor from './lib/sensor.js';

dotenv.config();

const ENV = process.env.NODE_ENV;
const HOST = process.env.HOST;
const PORT = process.env.PORT;

const app = express();

app.use(express.static('public'));

app.get('/read', function (req, res, next) {
	let readout = sensor.read();
	res.send({
		temperature: readout.temperature,
		humidity: readout.humidity
	});
});

app.listen(PORT, function () {
	console.log(`tempy is running in '${ENV}' environment! See: http://${HOST}:${PORT}`);
});