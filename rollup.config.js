import replace from '@rollup/plugin-replace';
import dotenv from 'dotenv';

dotenv.config();


export default [{
    input: 'src/client.js',
    output: {
        sourcemap: true,
        format: 'iife',
        name: 'client',
        file: 'public/build/client.js'
    },
    plugins: [
        replace({
            _process: JSON.stringify({
                env: {
                    HOST: process.env.HOST,
                    PORT: process.env.PORT,
                    INTERVAL: process.env.INTERVAL
                }
            }),
        }),
    ],
}, {
    input: 'src/server.js',
    output: {
        format: 'cjs',
        name: 'server',
        file: 'dist/server.js'
    }
}];
