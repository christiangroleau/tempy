# tempy

![Release](https://img.shields.io/badge/Release-2.0-blue?labelColor=grey&style=flat)
![Node](https://img.shields.io/badge/Node-10.24+-blue?labelColor=grey&style=flat)
![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)

The super simple Raspberry Pi temperature monitor

![tempy](tempy.png)

## Requirements

The following is required:

**Hardware**
- Raspberry Pi with 40 pin GPIO headers
- [DHT22 temperature sensor](https://www.pishop.ca/product/am2302-wired-dht22-temperature-humidity-sensor/)

**Software**
- [node.js](https://pimylifeup.com/raspberry-pi-nodejs/) (minimum v10.24)



## Wiring

The DHT22 has three pins **5V**, **Ground** and **Data** and is connected according to this table

DHT22 | Label | Raspberry Pi GPIO |
--- | --- | --- |
5 Volt | '+' or '5V' | Pin 2 |
Ground | '-' or 'GND' | Pin 6 | 
Data | 'out' or 'data' | Pin 7 |

Or, in picture form

![wiring](wiring.jpg)


*The sensor is optional for testing as tempy may be run in development mode and will display fake data. (see below)*

## Getting started (local development)

Install the dependencies

```
npm install
```

*Development mode* is configured through a `.env` file

```
touch .env
```
Edit the file for development mode (see .env.example)

```
NODE_ENV=development
HOST=localhost
PORT=3000
INTERVAL=10000
```

**Note:** In this mode, the sensor is not needed as the server will simply respond with fake readings

Now, run the application

```
npm run start
```

And go to http://localhost:3000 (or however it was configured in `.env`)

Wait 10 seconds (default) for the first readout

## Getting started (production)

*Production mode* is configured using the same `.env` file

```
NODE_ENV=production
HOST=rasperrypi
PORT=3000
INTERVAL=10000
```

In this mode data will be read from the DHT22 sensor

## Configuration

The following configurations are available

```
NODE_ENV=production
HOST=rasperrypi
PORT=3000
INTERVAL=10000
SCALE=C
```

**INTERVAL** seconds between sensor reads

**SCALE** display Celcius (C) or Fahrenheit (F)


## Keep it running

Keep the server up with [pm2](https://github.com/Unitech/pm2)

```
npm install -g pm2
pm2 start 'npm run start'
```

The following will be printed to screen (or similar)

```
[PM2] Starting /home/pi/tempy/server.js in fork_mode (1 instance)
[PM2] Done.
┌─────┬──────────────────┬─────────────┬─────────┬─────────┬──────────┬────────┬──────┬───────────┬──────────┬──────────┬──────────┬──────────┐
│ id  │ name             │ namespace   │ version │ mode    │ pid      │ uptime │ ↺    │ status    │ cpu      │ mem      │ user     │ watching │
├─────┼──────────────────┼─────────────┼─────────┼─────────┼──────────┼────────┼──────┼───────────┼──────────┼──────────┼──────────┼──────────┤
│ 0   │ npm run start    │ default     │ N/A     │ fork    │ 19442    │ 1s     │ 0    │ online    │ 0%       │ 18.3mb   │ pi       │ disabled │
└─────┴──────────────────┴─────────────┴─────────┴─────────┴──────────┴────────┴──────┴───────────┴──────────┴──────────┴──────────┴──────────┘
```

The application is now viewable on http://raspberrypi:3000

*Note:* The hostname may be different and is configurable in the `.env` file

## Shutting down
Get the id listed by pm2 and use it in the `stop` command

```
pm2 status
pm2 stop <id>
```

## Dependency

tempy is able to read sensor data using the awesome JavaScript library [node-dht-sensory](https://www.npmjs.com/package/node-dht-sensor)

**Node v10+** is supported for users of the first generation Raspberry Pi 0W onto which it can be difficult to install a more recent version. With this older version of Node expect to see warnings however with no loss in functionality.

